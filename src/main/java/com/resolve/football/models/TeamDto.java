/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.football.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Team DataTransferObject.
 *
 * @author Rommel Medina
 *
 */
public class TeamDto {
  /***
   * Unique Id.
   */
  private String id;
  
  /**
   * Team name.
   */
  private String name;
  
  /**
   * Current players.
   */
  private List<PlayerDto> players;
  
  /**
   * Team production.
   */
  private Double production;

  /**
   * Team's id.
   * @return id Team's id. 
   */
  public String getId() {
    return id;
  }

  /**
   * Set Team's id.
   * @param id Team's id.
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Team's name.
   * @return name Team's name.
   */
  public String getName() {
    return name;
  }

  /**
   * Set Team's name.
   * @param name Team's name.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Team player's.
   * @return players Team player's.
   */
  public List<PlayerDto> getPlayers() {
    return players;
  }

  /**
   * Set Team player's.
   * @param players Team player's list.
   */
  public void setPlayers(List<PlayerDto> players) {
    this.players = players;
  }

  /**
   * Team's scored goals production.
   * @return production Team's scored goals production.
   */
  public Double getProduction() {
    return production;
  }

  /**
   * Set Team's scored goals production.
   * @param production Team's scored goals production.
   */
  public void setProduction(Double production) {
    this.production = production;
  }
  
  /**
   * Add new player.
   * @param name
   * @param level
   * @param team
   * @param fixedSalary
   * @param bonus
   * @param scoredPoints
   */
  public void addPlayer(
      String name,
      String level,
      String team,
      Double fixedSalary,
      Double bonus,
      Double scoredPoints
      ) {
    if (this.players == null) {
      this.players = new ArrayList<PlayerDto>();
    }
    PlayerDto player = new PlayerDto();
    player.setId(name);
    player.setName(name);
    player.setLevel(level);
    player.setTeam(team);
    player.setFixedSalary(fixedSalary);
    player.setBonus(bonus);
    player.setScoredPoints(scoredPoints);
    this.players.add(player);
  }
  
  /**
   * Search player by name.
   * @param name Payer's name.
   * @return player
   */
  public PlayerDto getPlayer(String name) {
    String searched = name.trim().toLowerCase();
    return this.players.stream()
        .filter(player -> searched.equals(player.getName().trim().toLowerCase()))
        .findAny()
        .orElse(null);
  }
}
