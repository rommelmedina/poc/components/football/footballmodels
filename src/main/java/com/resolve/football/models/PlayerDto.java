/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.football.models;

/**
 * Data transfer object for Player model.
 *
 * @author Rommel Medina
 *
 */
public class PlayerDto {
  /**
   * Unique id.
   */
  private String id;

  /**
   * Player name.
   */
  private String name;

  /**
   * Player level.
   */
  private String level;

  /**
   * Player team.
   */
  private String team;

  /**
   * Player fixed salary.
   */
  private Double fixedSalary;

  /**
   * Player bonus.
   */
  private Double bonus;

  /**
   * Player final Salary.
   */
  private Double fullSalary;

  /**
   * Scored goals.
   */
  private Double scoredPoints;

  /**
   * Player's id.
   *
   * @return id Player's id.
   */
  public String getId() {
    return id;
  }

  /**
   * Set Player's id.
   *
   * @param id Player's id.
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Player name.
   *
   * @return name
   */
  public String getName() {
    return name;
  }

  /**
   * Player name.
   *
   * @param name the player's name.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Player's category level.
   *
   * @return level
   */
  public String getLevel() {
    return level;
  }

  /**
   * Set player's level.
   *
   * @param level player's level.
   */
  public void setLevel(String level) {
    this.level = level;
  }

  /**
   * Player's team.
   *
   * @return team Player's team name.
   */
  public String getTeam() {
    return team;
  }

  /**
   * Set player's team.
   *
   * @param team Team name.
   */
  public void setTeam(String team) {
    this.team = team;
  }

  /**
   * Player's salary base.
   *
   * @return fixedSalary Player's salary base.
   */
  public Double getFixedSalary() {
    return fixedSalary;
  }

  /**
   * Set player's salary base.
   *
   * @param fixedSalary Player's salary base.
   */
  public void setFixedSalary(Double fixedSalary) {
    this.fixedSalary = fixedSalary;
  }

  /**
   * Player's bonus amount.
   *
   * @return bonus Player's bonus amount.
   */
  public Double getBonus() {
    return bonus;
  }

  /**
   * Set player's bonus amount.
   *
   * @param bonus Player's bonus amount.
   */
  public void setBonus(Double bonus) {
    this.bonus = bonus;
  }

  /**
   * Net earnings. Complete salary include all additional bonus.
   *
   * @return fullSalary Complete salary.
   */
  public Double getFullSalary() {
    return fullSalary;
  }

  /**
   * Set complete salary, including all additional bonus.
   *
   * @param fullSalary Complete salary.
   */
  public void setFullSalary(Double fullSalary) {
    this.fullSalary = fullSalary;
  }

  /**
   * Player's scored points.
   *
   * @return scoredPoints Player's scored points.
   */
  public Double getScoredPoints() {
    return scoredPoints;
  }

  /**
   * Set player's scored points.
   *
   * @param scoredPoints Player's scored points.
   */
  public void setScoredPoints(Double scoredPoints) {
    this.scoredPoints = scoredPoints;
  }
}